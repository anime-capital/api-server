﻿using System;
using AnimeCapital.AnimeDomain.Repository;
using AnimeCapital.AnimeDomain.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.EntityFrameworkCore;

namespace AnimeCapital
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }

        public IHostingEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (Env.IsDevelopment())
            {
                const string connection =
                    @"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0";
                services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(connection));
            }
            else
            {
                var connection =
                    $"Host=db;Database=anime;Username=postgres;Password={Environment.GetEnvironmentVariable("DB_PASSWORD")}";
                services.AddDbContext<DatabaseContext>(options => options.UseNpgsql(connection));
            }

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info {Title = "Anime Capital API", Version = "v1"}));
            services.AddCors(o => o.AddPolicy("GetPolicy", builder =>
            {
                builder.WithMethods("GET")
                    .AllowAnyOrigin()
                    .AllowAnyHeader();
            }));
            services.AddScoped<IAnimeService, AnimeService>();
            services.AddScoped<IAnimeRepository, AnimeRepository>();
            services.AddScoped<IMyAnimeListService, MyAnimeListService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseCors("GetPolicy");
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });

            using (var serviceScope =
                app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DatabaseContext>();
                if (env.IsDevelopment()) context.Database.EnsureDeleted();
                context.Database.Migrate();
            }

            app.UseMvc();
        }
    }
}