﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Kirinnee.Helper;
using Microsoft.AspNetCore.Mvc;

namespace AnimeCapital.Video
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideoController : ControllerBase
    {
        private readonly HttpClient _http;

        public VideoController()
        {
            _http = new HttpClient();
        }

        // GET api/Video
        [HttpGet("{organization}/{project}/{repo}/{branch}/{folder}/{file}")]
        public async Task<object> Get(string organization, string project, string repo, string branch,
            string folder, string file)
        {
            var target =
                $"https://dev.azure.com/{organization}/{project}/_apis/git/repositories/{repo}/Items?path=%2F{folder}%2F{file}&versionDescriptor%5BversionOptions%5D=0&versionDescriptor%5BversionType%5D=0&versionDescriptor%5Bversion%5D={branch}&download=true&resolveLfs=true&%24format=octetStream&api-version=5.0-preview.1";
            if (file.SplitBy(".").Last(1).ToArray()[0] != "mpd") return Redirect(target);
            var content = await _http.GetStringAsync(target);
            return content;
        }

        [HttpHead("{organization}/{project}/{repo}/{branch}/{folder}/{file}")]
        public async Task<IActionResult> Head(string organization, string project, string repo, string branch,
            string folder, string file)
        {
            var target =
                $"https://dev.azure.com/{organization}/{project}/_apis/git/repositories/{repo}/Items?path=%2F{folder}%2F{file}&versionDescriptor%5BversionOptions%5D=0&versionDescriptor%5BversionType%5D=0&versionDescriptor%5Bversion%5D={branch}&download=true&resolveLfs=true&%24format=octetStream&api-version=5.0-preview.1";
            var content = await _http.GetAsync(target);
            if (content.IsSuccessStatusCode)
            {
                return Ok();
            }
            return NotFound();
        }
    }
}