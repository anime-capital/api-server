using AnimeCapital.AnimeDomain.Data;
using Microsoft.EntityFrameworkCore;

namespace AnimeCapital
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AnimeDataModel>()
                .HasMany(c => c.Episodes)
                .WithOne(e => e.Anime);
        }

        public DbSet<AnimeDataModel> Animes { get; set; }
        public DbSet<EpisodeDataModel> Episodes { get; set; }
    }
}