﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimeCapital.Migrations
{
    public partial class DateTimeToDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LatestUpdate",
                table: "Animes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LatestUpdate",
                table: "Animes");
        }
    }
}
