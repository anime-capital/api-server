﻿// <auto-generated />
using System;
using AnimeCapital;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AnimeCapital.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    partial class DatabaseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10034")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AnimeCapital.AnimeDomain.Data.AnimeDataModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Genre");

                    b.Property<string>("Image");

                    b.Property<DateTime>("LatestUpdate");

                    b.Property<string>("Name");

                    b.Property<long>("Popularity");

                    b.Property<decimal>("Rating")
                        .HasColumnType("decimal(3, 1)");

                    b.Property<string>("Related");

                    b.Property<string>("Synopsis");

                    b.HasKey("Id");

                    b.ToTable("Animes");
                });

            modelBuilder.Entity("AnimeCapital.AnimeDomain.Data.EpisodeDataModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AnimeId");

                    b.Property<string>("Mkv");

                    b.Property<string>("Mp4");

                    b.Property<string>("Mpd");

                    b.Property<string>("Name");

                    b.Property<int>("Number");

                    b.Property<DateTime>("Updated");

                    b.HasKey("Id");

                    b.HasIndex("AnimeId");

                    b.ToTable("Episodes");
                });

            modelBuilder.Entity("AnimeCapital.AnimeDomain.Data.EpisodeDataModel", b =>
                {
                    b.HasOne("AnimeCapital.AnimeDomain.Data.AnimeDataModel", "Anime")
                        .WithMany("Episodes")
                        .HasForeignKey("AnimeId");
                });
#pragma warning restore 612, 618
        }
    }
}
