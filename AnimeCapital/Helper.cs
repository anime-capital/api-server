using System.Collections.Generic;
using Kirinnee.Helper;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.Internal;

namespace AnimeCapital
{
    public static class Helper
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable) {
            return enumerable == null || !enumerable.Any();
        }

        public static string UrlEncode(this string input)
        {
            return input.SplitBy(" ").JoinBy("%20");
        }
        
        public static TValue GetValueOrDefault<TKey, TValue>
            (   this IDictionary<TKey, TValue> dictionary,TKey key)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }
    }
}