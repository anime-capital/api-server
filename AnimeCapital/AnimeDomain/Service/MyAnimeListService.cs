using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Domain;
using AnimeCapital.AnimeDomain.Response;
using HtmlAgilityPack;
using Kirinnee.Helper;
using Newtonsoft.Json;
using ScrapySharp.Extensions;

namespace AnimeCapital.AnimeDomain.Service
{
    public class MyAnimeListService : IMyAnimeListService
    {
        private readonly HttpClient _http = new HttpClient();

        private async Task<T> Ping<T>(string endpoint)
        {
            var resp = await _http.GetStringAsync(endpoint);
            return JsonConvert.DeserializeObject<T>(resp);
        }

        public async Task<AnimeInfo> GetAnimeInfo(string name)
        {
            var endpoint =
                $"https://myanimelist.net/search/prefix.json?type=all&keyword={name.UrlEncode()}&v=1";
            var response = await Ping<MyAnimeListResponseModel>(endpoint);
            var data = response.categories?[0]?.items?[0];

            var url = data.GetImageUrl();
            var rating = data.payload.score;

            var document = await _http.GetStringAsync(data.url);
            var html = new HtmlDocument();
            html.LoadHtml(document);
            var doc = html.DocumentNode;

            var episodes = await GetAllEpisode(data.url);
            return new AnimeInfo
            {
                Image = new Uri(url),
                Rating = Convert.ToDecimal(rating),
                Genre = GetGenre(doc),
                Synopsis = GetSynopsis(doc),
                Episodes = episodes,
                SearchQuery = GetRelatedName(doc),
                Popularity = GetPopularity(doc)
            };
        }

        private IEnumerable<string> GetRelatedName(HtmlNode node)
        {
            var english = GetValue(node, "English:");
            var japanese = GetValue(node, "Japanese:");
            var synonym = GetValue(node, "Synonyms:");
            var ret = new LinkedList<string>();
            if (english != null) ret.AddLast(english);
            if (japanese != null) ret.AddLast(japanese);
            return synonym == null ? ret : ret.Concat(synonym.SplitBy(",")).Select(e => e.Trim());
        }

        private int GetPopularity(HtmlNode node)
        {
            var pop = GetValue(node, "Popularity:");
            return pop?.Skip(1).ToInt() ?? 9999999;
        }

        private IEnumerable<string> GetGenre(HtmlNode node)
        {
            var g = GetValue(node, "Genres:");
            return g == null ? new string[] { } : g.SplitBy(",").Select(e => e.Trim());
        }

        private string GetSynopsis(HtmlNode node)
        {
            var synopsis = node.CssSelect("h2").First(e => e.InnerText.EndsWith("Synopsis"));
            var parent = synopsis.ParentNode;
            var first = parent.FirstChild;
            first.Remove();
            synopsis.Remove();
            return parent.CssSelect("span").First().InnerText.SplitBy("[Written by MAL Rewrite]").JoinBy().Trim();
        }

        private async Task<Dictionary<int, string>> GetAllEpisode(string url, int count = 0)
        {
            try
            {
                var response = await _http.GetStringAsync($"{url}/episode?offset={count}");
                var doc = new HtmlDocument();
                doc.LoadHtml(response);
                var ep = doc.DocumentNode;
                var episodes = GetEpisode(ep);
                if (episodes.Count == 100)
                {
                    var nextHundred = await GetAllEpisode(url, count + 100);
                    return episodes.Concat(nextHundred).ToDictionary(x => x.Key, x => x.Value);
                }

                return episodes;
            }
            catch
            {
                return new Dictionary<int, string>();
            }
        }

        private Dictionary<int, string> GetEpisode(HtmlNode node)
        {
            return node.CssSelect(".mt8.episode_list.ascend td.episode-number.nowrap")
                .ToDictionary(e => e.InnerText.ToInt(),
                    e => e.ParentNode.CssSelect(".episode-title a").First().InnerText);
        }

        private string GetValue(HtmlNode node, string label)
        {
            var key = node.CssSelect(".dark_text").FirstOrDefault(e => e.InnerText == label);
            if (key == null) return null;
            var parent = key.ParentNode;
            key.Remove();
            return parent.InnerText.Trim();
        }
    }
}