using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Domain;

namespace AnimeCapital.AnimeDomain.Service
{
    public interface IMyAnimeListService
    {
        Task<AnimeInfo> GetAnimeInfo(string name);
    }

    
}