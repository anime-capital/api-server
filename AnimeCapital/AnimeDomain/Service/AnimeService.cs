using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Domain;
using AnimeCapital.AnimeDomain.Repository;
using AnimeCapital.AnimeDomain.Request;
using AnimeCapital.AnimeDomain.Response;

namespace AnimeCapital.AnimeDomain.Service
{
    public class AnimeService : IAnimeService
    {
        private readonly IAnimeRepository _repository;
        private readonly IMyAnimeListService _myAnimeList;

        public AnimeService(IAnimeRepository repository, IMyAnimeListService myAnimeList)
        {
            _repository = repository;
            _myAnimeList = myAnimeList;
        }


        public async Task<string> AddEpisode(AddEpisodeRequestModel request)
        {
            try
            {
                var anime = await _repository.GetAnimeByName(request.AnimeName);
                var info = await _myAnimeList.GetAnimeInfo(request.AnimeName);
                if (anime == null)
                {
                    anime = await _repository.CreateAnime(request.AnimeName, info.Image, info.Genre, info.Rating,
                        info.Synopsis, info.SearchQuery, info.Popularity);
                }

                var episodeName = info.Episodes.GetValueOrDefault(request.EpisodeNumber) ??
                                  $"{request.AnimeName} Episode {request.EpisodeNumber}";
                var org = request.Organization.UrlEncode();
                var project = request.Project.UrlEncode();
                var repo = request.AnimeName.UrlEncode();
                var file = $"Episode {request.EpisodeNumber}%2Foutput";
                var mpdPath = $"{file}.mpd".UrlEncode();
                var mkvPath = $"{file}.mkv".UrlEncode();
                var mp4Path = $"{file}.mp4".UrlEncode();
                var mpdLink = $"/api/Video/{org}/{project}/{repo}/master/{mpdPath}";
                var mkvLink = $"/api/Video/{org}/{project}/{repo}/master/{mkvPath}";
                var mp4Link = $"/api/Video/{org}/{project}/{repo}/master/{mp4Path}";
                var episode =
                    await _repository.CreateEpisode(episodeName, request.EpisodeNumber, mpdLink, mp4Link, mkvLink);
                var success = await _repository.AddEpisode(anime, episode);
                return success ? null : "Failed adding relationship";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> AddOldEpisode(AddEpisodeRequestModel request)
        {
            try
            {
                var anime = await _repository.GetAnimeByName(request.AnimeName);
                var info = await _myAnimeList.GetAnimeInfo(request.AnimeName);
                if (anime == null)
                {
                    return $"Cannot find anime {request.AnimeName}, therefore not old!";
                }

                var episodeName = info.Episodes.GetValueOrDefault(request.EpisodeNumber) ??
                                  $"{request.AnimeName} Episode {request.EpisodeNumber}";
                var org = request.Organization.UrlEncode();
                var project = request.Project.UrlEncode();
                var repo = request.AnimeName.UrlEncode();
                var file = $"Episode {request.EpisodeNumber}/output";
                var mpdPath = $"{file}.mpd".UrlEncode();
                var mkvPath = $"{file}.mkv".UrlEncode();
                var mp4Path = $"{file}.mp4".UrlEncode();
                var mpdLink = $"/api/Video/{org}/{project}/{repo}/master/{mpdPath}";
                var mkvLink = $"/api/Video/{org}/{project}/{repo}/master/{mkvPath}";
                var mp4Link = $"/api/Video/{org}/{project}/{repo}/master/{mp4Path}";
                var episode =
                    await _repository.CreateOldEpisode(anime, episodeName, request.EpisodeNumber, mpdLink, mp4Link,
                        mkvLink);
                var success = await _repository.AddOldEpisode(anime, episode);
                return success ? null : "Failed adding relationship";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<IEnumerable<AnimeFlyweightModel>> GetAll(int page, int amount)
        {
            var animes = await _repository.GetAnimeFrom(page, amount);
            return animes.Select(ToFlyWeightModel);
        }

        public async Task<IEnumerable<AnimeFlyweightModel>> GetAnimeByFilter(string name, IEnumerable<string> genre)
        {
            var animes = await _repository.Filter(name, genre);
            return animes.Select(ToFlyWeightModel);
        }

        public async Task<AnimeResponseModel> GetAnime(Guid id)
        {
            var anime = await _repository.GetAnimeById(id);
            return ToAnimeReponseModel(anime);
        }

        public async Task UpdateAllAnime()
        {
            var animes = await _repository.GetAllAnime();
            foreach (var anime in animes)
            {
                var info = await _myAnimeList.GetAnimeInfo(anime.Name);
                anime.Genre = info.Genre;
                anime.Rating = info.Rating;
                anime.Image = info.Image;
                anime.Synopsis = info.Synopsis;
                anime.Related = info.SearchQuery;
                anime.Popularity = info.Popularity;
                await _repository.UpdateAnime(anime);
                await Task.Delay(20000);
            }
        }

        private AnimeFlyweightModel ToFlyWeightModel(Anime e)
        {
            return new AnimeFlyweightModel(e.Id, e.Name, e.Image, e.Genre, e.Rating, e.Synopsis,
                e.LatestUpdate, e.Episodes.Max(x => x.Number));
        }

        private AnimeResponseModel ToAnimeReponseModel(Anime e)
        {
            return new AnimeResponseModel(e.Id, e.Name, e.Image, e.Genre, e.Rating, e.Synopsis
                , e.Episodes.Select(ToEpisodeResponseModel));
        }

        private EpisodeResponseModel ToEpisodeResponseModel(Episode e)
        {
            return new EpisodeResponseModel(e.Updated, e.Name, e.Number, e.StreamLink, e.Mp4Link, e.MkvLink);
        }
    }
}