using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Request;
using AnimeCapital.AnimeDomain.Response;

namespace AnimeCapital.AnimeDomain.Service
{
    public interface IAnimeService
    {
        Task<string> AddEpisode(AddEpisodeRequestModel request);
        Task<string> AddOldEpisode(AddEpisodeRequestModel request);
        Task<IEnumerable<AnimeFlyweightModel>> GetAll(int page, int amount);
        Task<IEnumerable<AnimeFlyweightModel>> GetAnimeByFilter(string name, IEnumerable<string> genre);
        Task<AnimeResponseModel> GetAnime(Guid id);
        Task UpdateAllAnime();
    }
}