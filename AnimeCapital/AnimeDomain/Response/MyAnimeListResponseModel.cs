using System.Linq;
using Kirinnee.Helper;

namespace AnimeCapital.AnimeDomain.Response
{
    public class MyAnimeListResponseModel
    {
        public MyAnimeListCategory[] categories { get; set; }
    }

    public class MyAnimeListCategory
    {
        public string type { get; set; }
        public MyAnimeListItem[] items { get; set; }
    }

    public class MyAnimeListItem
    {
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string image_url { get; set; }
        private string thumbnail_url { get; set; }
        private string es_score { get; set; }
        public MyAnimeListPayload payload { get; set; }

        public string GetImageUrl()
        {
            var keyValues = image_url.SplitBy(".")
                .Omit(1)
                .Last(1)
                .ToArray()[0]
                .SplitBy("/")
                .Last(2)
                .Select(e => e.ToInt())
                .ToArray();
            return $"https://cdn.myanimelist.net/images/anime/{keyValues[0]}/{keyValues[1]}.jpg";
        }
    }

    public class MyAnimeListPayload
    {
        public string media_type { get; set; }
        public int start_year { get; set; }
        public string aired { get; set; }
        public string score { get; set; }
        public string status { get; set; }
    }
}