using System;
using System.Collections.Generic;

namespace AnimeCapital.AnimeDomain.Response
{
    public class AnimeResponseModel
    {
        public Guid Id;
        public string Name;
        public Uri Image;
        public IEnumerable<string> Genre;
        public decimal Rating;
        public string Synopsis;
        public IEnumerable<EpisodeResponseModel> Episodes;

        public AnimeResponseModel(Guid id, string name, Uri image, IEnumerable<string> genre, decimal rating,
            string synopsis, IEnumerable<EpisodeResponseModel> episodes)
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Image = image ?? throw new ArgumentNullException(nameof(image));
            Genre = genre ?? throw new ArgumentNullException(nameof(genre));
            Rating = rating;
            Synopsis = synopsis ?? throw new ArgumentNullException(nameof(synopsis));
            Episodes = episodes ?? throw new ArgumentNullException(nameof(episodes));
        }
    }

    public class EpisodeResponseModel
    {
        public DateTime Updated;
        public string Name;
        public int Number;
        public string StreamLink;
        public string Mp4Link;
        public string MkvLink;

        public EpisodeResponseModel(DateTime updated, string name, int number, string streamLink, string mp4Link,
            string mkvLink)
        {
            Updated = updated;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Number = number;
            StreamLink = streamLink ?? throw new ArgumentNullException(nameof(streamLink));
            Mp4Link = mp4Link ?? throw new ArgumentNullException(nameof(mp4Link));
            MkvLink = mkvLink ?? throw new ArgumentNullException(nameof(mkvLink));
        }
    }
}