using System;
using System.Collections.Generic;

namespace AnimeCapital.AnimeDomain.Response
{
    public struct AnimeFlyweightModel
    {
        public Guid Id;
        public string Name;
        public Uri Image;
        public IEnumerable<string> Genre;
        public decimal Rating;
        public string Synopsis;
        public DateTime Updated;
        public int Episode;

        public AnimeFlyweightModel(Guid id, string name, Uri image, IEnumerable<string> genre, decimal rating, string synopsis, DateTime updated, int episode)
        {
            Id = id;
            Name = name;
            Image = image;
            Genre = genre;
            Rating = rating;
            Synopsis = synopsis;
            Updated = updated;
            Episode = episode;
        }
    }
}