using System;
using System.Collections.Generic;

namespace AnimeCapital.AnimeDomain.Domain
{
    public class AnimeInfo
    {
        public Uri Image { get; set; }
        public decimal Rating { get; set; }
        public IEnumerable<string> Genre { get; set; }
        public string Synopsis { get; set; }
        public Dictionary<int, string> Episodes { get; set; }
        public IEnumerable<string> SearchQuery { get; set; }
        public long Popularity { get; set; }
    }
}