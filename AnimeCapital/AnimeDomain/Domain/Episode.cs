using System;

namespace AnimeCapital.AnimeDomain.Domain
{
    public class Episode
    {
        public Guid Id { get; }
        public DateTime Updated { get; }
        public string Name { get; }
        public int Number { get; }
        public string StreamLink { get; }
        public string Mp4Link { get; }
        public string MkvLink { get; }

        public Episode(Guid id, DateTime updated, string name, int number, string streamLink, string mp4Link,
            string mkvLink)
        {
            Id = id;
            Updated = updated;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Number = number;
            StreamLink = streamLink ?? throw new ArgumentNullException(nameof(streamLink));
            Mp4Link = mp4Link ?? throw new ArgumentNullException(nameof(mp4Link));
            MkvLink = mkvLink ?? throw new ArgumentNullException(nameof(mkvLink));
        }
    }
}