using System;
using System.Collections;
using System.Collections.Generic;

namespace AnimeCapital.AnimeDomain.Domain
{
    public class Anime
    {
        public Guid Id { get; }
        public Uri Image { get; set;  }
        public string Name { get; }
        public DateTime LatestUpdate { get; }
        public IEnumerable<string> Genre { get; set; }
        public decimal Rating { get; set;  }
        public string Synopsis { get; set;  }
        public IEnumerable<Episode> Episodes { get; }
        public IEnumerable<string> Related { get; set; }
        public long Popularity { get; set; }

        public Anime(Guid id, Uri image, string name, DateTime latestUpdate, IEnumerable<string> genre,
            decimal rating, string synopsis, IEnumerable<Episode> episodes, IEnumerable<string> related,
            long popularity)
        {
            Id = id;
            Image = image;
            Name = name;
            LatestUpdate = latestUpdate;
            Genre = genre;
            Rating = rating;
            Synopsis = synopsis;
            Episodes = episodes;
            Related = related;
            Popularity = popularity;
        }
    }
}