using System;

namespace AnimeCapital.AnimeDomain.Data
{
    public class EpisodeDataModel
    {
        public Guid Id { get; set; }
        public DateTime Updated { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public string Mpd { get; set; }
        public string Mkv { get; set; }
        public string Mp4 { get; set; }
        public AnimeDataModel Anime { get; set; }
    }
}