using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimeCapital.AnimeDomain.Data
{
    public class AnimeDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Genre { get; set; }
        public string Related { get; set; }
        [Column(TypeName = "decimal(3, 1)")] public decimal Rating { get; set; }
        public string Synopsis { get; set; }
        public long Popularity { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime LatestUpdate { get; set; }
        public List<EpisodeDataModel> Episodes { get; } = new List<EpisodeDataModel>();
    }
}