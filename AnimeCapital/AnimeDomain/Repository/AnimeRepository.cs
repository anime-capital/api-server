using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Data;
using AnimeCapital.AnimeDomain.Domain;
using Kirinnee.Helper;
using Microsoft.EntityFrameworkCore;

namespace AnimeCapital.AnimeDomain.Repository
{
    public class AnimeRepository : IAnimeRepository
    {
        private readonly DatabaseContext _db;

        public AnimeRepository(DatabaseContext db)
        {
            _db = db;
        }

        public async Task<Anime> GetAnimeById(Guid id)
        {
            var dataModel = await _db.Animes.Include(e => e.Episodes).SingleOrDefaultAsync(e => e.Id == id);
            return dataModel == null ? null : ToAnimeDomainModel(dataModel);
        }

        public async Task<Anime> GetAnimeByName(string name)
        {
            var dataModel = await _db.Animes.Include(e => e.Episodes).SingleOrDefaultAsync(e => e.Name == name);
            return dataModel == null ? null : ToAnimeDomainModel(dataModel);
        }

        public Task<IEnumerable<Anime>> GetAllAnime()
        {
            var animes = _db.Animes.Include(e => e.Episodes);
            return Task.FromResult(animes.AsEnumerable().Select(ToAnimeDomainModel));
        }

        public Task<IEnumerable<Anime>> GetAnimeFrom(int page, int amount)
        {
            var animes = _db.Animes.Include(e => e.Episodes).OrderByDescending(e => e.LatestUpdate).Skip(page * amount)
                .Take(amount);
            return Task.FromResult(animes.AsEnumerable().Select(ToAnimeDomainModel));
        }

        public Task<IEnumerable<Anime>> Filter(string search, IEnumerable<string> genre)
        {
            var enumerable = genre as string[] ?? genre?.ToArray();
            var filtered = _db.Animes.Include(e => e.Episodes)
                .Where(e => search == null || Match(e.Name, search) || Match(e.Related, search))
                .Where(e => enumerable.IsNullOrEmpty() || e.Genre.SplitBy(",").All(enumerable.Contains))
                .OrderBy(e => e.Popularity);
            return Task.FromResult(filtered.AsEnumerable().Select(ToAnimeDomainModel));
        }

        public async Task<Anime> CreateAnime(string name, Uri image, IEnumerable<string> genre, decimal rating,
            string synopsis, IEnumerable<string> related, long popularity)
        {
            var id = Guid.NewGuid();
            var dataModel = new AnimeDataModel
            {
                Id = id,
                Name = name,
                Image = image.ToString(),
                Genre = genre.JoinBy(","),
                Rating = rating,
                Synopsis = synopsis,
                Related = related.JoinBy("^%$#@!,"),
                Popularity = popularity,
                LatestUpdate = DateTime.Now
            };
            var addedModel = await _db.AddAsync(dataModel);
            await _db.SaveChangesAsync();
            return ToAnimeDomainModel(addedModel.Entity);
        }

        public async Task<Anime> UpdateAnime(Anime anime)
        {
            var animeData = await _db.Animes.Include(e => e.Episodes).SingleOrDefaultAsync(e => e.Id == anime.Id);
            animeData.Genre = anime.Genre.JoinBy(",");
            animeData.Rating = anime.Rating;
            animeData.Image = anime.Image.ToString();
            animeData.Synopsis = anime.Synopsis;
            animeData.Related = anime.Related.JoinBy("^%$#@!,");
            animeData.Popularity = anime.Popularity;
            await _db.SaveChangesAsync();
            return anime;
        }

        public async Task<Episode> CreateEpisode(string name, int number, string mpd, string mp4, string mkv)
        {
            var id = Guid.NewGuid();
            var dataModel = new EpisodeDataModel
            {
                Id = id,
                Name = name,
                Number = number,
                Mpd = mpd,
                Mkv = mkv,
                Mp4 = mp4,
                Updated = DateTime.Now
            };
            var addedModel = await _db.AddAsync(dataModel);
            await _db.SaveChangesAsync();
            return ToEpisodeDomainModel(addedModel.Entity);
        }

        public async Task<Episode> CreateOldEpisode(Anime anime, string name, int number, string mpd, string mp4,
            string mkv)
        {
            var episode = anime.Episodes.First(e => e.Number == number + 1);
            if (episode == null) return null;
            var id = Guid.NewGuid();
            var dataModel = new EpisodeDataModel
            {
                Id = id,
                Name = name,
                Number = number,
                Mpd = mpd,
                Mkv = mkv,
                Mp4 = mp4,
                Updated = episode.Updated.AddDays(-7)
            };
            var addedModel = await _db.AddAsync(dataModel);
            await _db.SaveChangesAsync();
            return ToEpisodeDomainModel(addedModel.Entity);
        }

        public async Task<bool> AddOldEpisode(Anime anime, Episode episode)
        {
            try
            {
                var animeData = await _db.Animes.Include(e => e.Episodes).FirstOrDefaultAsync(e => e.Id == anime.Id);
                var episodeData = await _db.Episodes.FirstOrDefaultAsync(e => e.Id == episode.Id);
                animeData.Episodes.Add(episodeData);
                await _db.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }

        public async Task<bool> AddEpisode(Anime anime, Episode episode)
        {
            try
            {
                var animeData = await _db.Animes.Include(e => e.Episodes).FirstOrDefaultAsync(e => e.Id == anime.Id);
                var episodeData = await _db.Episodes.FirstOrDefaultAsync(e => e.Id == episode.Id);
                animeData.Episodes.Add(episodeData);
                animeData.LatestUpdate = episodeData.Updated;
                await _db.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }

        private Episode ToEpisodeDomainModel(EpisodeDataModel model)
        {
            return new Episode(
                model.Id,
                model.Updated,
                model.Name,
                model.Number,
                model.Mpd,
                model.Mp4,
                model.Mkv
            );
        }

        private Anime ToAnimeDomainModel(AnimeDataModel model)
        {
            var episodes = model.Episodes.OrderByDescending(e => e.Updated).ToArray();
            return new Anime(
                model.Id,
                new Uri(model.Image),
                model.Name,
                model.LatestUpdate,
                model.Genre.Split(","),
                model.Rating,
                model.Synopsis,
                episodes.Select(ToEpisodeDomainModel),
                model.Related.SplitBy("^%$#@!,"),
                model.Popularity
            );
        }

        private bool Match(string search, string target)
        {
            var s = search.ToLower();
            var t = target.ToLower();
            return s.Contains(t) || t.Contains(s);
        }
    }
}