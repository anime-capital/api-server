using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Domain;

namespace AnimeCapital.AnimeDomain.Repository
{
    public interface IAnimeRepository
    {
        Task<Anime> GetAnimeById(Guid id);
        Task<Anime> GetAnimeByName(string name);

        Task<IEnumerable<Anime>> GetAllAnime();
        Task<IEnumerable<Anime>> GetAnimeFrom(int page, int amount);
        Task<IEnumerable<Anime>> Filter(string search, IEnumerable<string> genre);
        Task<bool> AddEpisode(Anime anime, Episode episode);

        Task<Anime> CreateAnime(string name, Uri image, IEnumerable<string> genre, decimal rating, string synopsis,
            IEnumerable<string> related, long popularity);

        Task<Anime> UpdateAnime(Anime anime);

        Task<Episode> CreateEpisode(string name, int number, string mpd, string mp4, string mkv);

        Task<Episode> CreateOldEpisode(Anime anime, string name, int number, string mpd, string mp4, string mkv);

        Task<bool> AddOldEpisode(Anime anime, Episode episode);
    }
}