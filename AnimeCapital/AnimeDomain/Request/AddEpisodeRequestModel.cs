using System.ComponentModel.DataAnnotations;

namespace AnimeCapital.AnimeDomain.Request
{
    public class AddEpisodeRequestModel
    {
        [Required] public string AnimeName;
        [Required] public int EpisodeNumber;
        [Required] public string Organization;
        [Required] public string Project;
    }
}