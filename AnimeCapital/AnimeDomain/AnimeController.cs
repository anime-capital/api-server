﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AnimeCapital.AnimeDomain.Domain;
using AnimeCapital.AnimeDomain.Request;
using AnimeCapital.AnimeDomain.Response;
using AnimeCapital.AnimeDomain.Service;
using Kirinnee.Helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AnimeCapital.AnimeDomain
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimeController : ControllerBase
    {
        private readonly IAnimeService _animeService;
        private readonly IMyAnimeListService _myAnimeList;
        private readonly IHostingEnvironment _env;
        private static ConcurrentDictionary<string, int> states = new ConcurrentDictionary<string, int>();

        public AnimeController(IAnimeService animeService, IMyAnimeListService myAnimeList, IHostingEnvironment env)
        {
            _animeService = animeService;
            _myAnimeList = myAnimeList;
            _env = env;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AnimeFlyweightModel>>> Get(int page = 0, int amount = 20)
        {
            var anime = await _animeService.GetAll(page, amount);
            return Ok(anime);
        }

        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<AnimeFlyweightModel>>> Filter(string name, string genre)
        {
            var genres = genre?.SplitBy(",");
            var anime = await _animeService.GetAnimeByFilter(name, genres);
            return Ok(anime);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AnimeResponseModel>> GetById(Guid id)
        {
            var anime = await _animeService.GetAnime(id);
            if (anime == null) return NotFound();
            return Ok(anime);
        }

        [HttpGet("{id}/{episode}")]
        public async Task<ActionResult<EpisodeResponseModel>> GetEpisodeById(Guid id, int episode)
        {
            var anime = await _animeService.GetAnime(id);
            if (anime == null) return NotFound();
            var ep = anime.Episodes.FirstOrDefault(e => e.Number == episode);
            if (ep == null) return NotFound();
            return Ok(ep);
        }

        [HttpGet("info/{name}")]
        public async Task<AnimeInfo> GetInfo(string name)
        {
            return await _myAnimeList.GetAnimeInfo(name);
        }

        [HttpPost]
        public async Task<ActionResult> AddEpisode([FromBody] AddEpisodeRequestModel payload)
        {
            if (!IsSigned(payload) && _env.IsProduction())
                return StatusCode(403, $@"Wrong Signature.");
            var success = await _animeService.AddEpisode(payload);
            if (success == null) return Ok();
            return BadRequest(success);
        }

        [HttpPost("old")]
        public async Task<ActionResult> AddOldEpisode([FromBody] AddEpisodeRequestModel payload)
        {
            if (!IsSigned(payload) && _env.IsProduction())
                return StatusCode(403, $@"Wrong Signature.");
            var success = await _animeService.AddOldEpisode(payload);
            if (success == null) return Ok();
            return BadRequest(success);
        }

        [HttpPatch]
        public ActionResult UpdateAllEpisode(string payload)
        {
            if (payload.Length < 20 && _env.IsProduction()) return StatusCode(403, "Not Allowed");
            if (!IsSigned(payload) && _env.IsProduction())
                return StatusCode(403, $@"Wrong Signature.");
            var ignored = _animeService.UpdateAllAnime();
            return Ok();
        }


        [HttpPost("status/{name}/{episode}/{state}")]
        public ActionResult SetEpisode(string name, int episode, int state)
        {
            var qualifiedName = $"{name}_{episode}";
            var signedData = qualifiedName + $"_{state}";
            if (!IsSigned(signedData) && _env.IsProduction()) return StatusCode(403, @"Wrong Signature.");
            states[qualifiedName] = state;
            return Ok();
        }

        [HttpGet("status/{name}/{episode}")]
        public ActionResult GetEpisode(string name, int episode)
        {
            var qualifiedName = $"{name}_{episode}";
            if (!IsSigned(qualifiedName) && _env.IsProduction()) return StatusCode(403, @"Wrong Signature.");
            if (!states.ContainsKey(qualifiedName)) states[qualifiedName] = 0;
            return Ok(states[qualifiedName]);
        }

        private bool IsSigned(object request)
        {
            var secret = Environment.GetEnvironmentVariable("SIGNER");
            var value = request is string ? request : JsonConvert.SerializeObject(request);
            var sha = Sha256(value + ";" + secret);
            var signature = Request.Headers["super-secret"];
            return sha == signature;
        }

        private static string Sha256(string input)
        {
            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input));
            foreach (var theByte in crypto) hash.Append(theByte.ToString("x2"));
            return hash.ToString();
        }
    }
}